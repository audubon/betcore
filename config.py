
import os


# configuration
SQLALCHEMY_DATABASE_URI ='sqlite:////tmp/test.db'
SQLALCHEMY_TRACK_MODIFICATIONS=True
DEBUG = True
SECRET_KEY = os.urandom(24)
